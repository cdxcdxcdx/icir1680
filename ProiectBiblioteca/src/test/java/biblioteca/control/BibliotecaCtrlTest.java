package biblioteca.control;

import static org.junit.Assert.*;

import java.time.Year;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;

public class BibliotecaCtrlTest {

	private BibliotecaCtrl ctrl;

	@Before
	public void setUp() throws Exception {
		CartiRepoInterface cartiRepo = new CartiRepoMock();
		ctrl = new BibliotecaCtrl(cartiRepo);
	}

	@Test
	public void testAdaugaCarte_TC1_EC() throws Exception {
		String titlu = "Carte Titlu";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 2010;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}

	@Test(expected = Exception.class)
	public void testAdaugaCarte_TC2_EC() throws Exception {
		String titlu = "";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 2010;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}

	@Test(expected = Exception.class)
	public void testAdaugaCarte_TC3_EC() throws Exception {
		String titlu = "Carte titlu";

		List<String> autori = null;

		int anAparitie = 2010;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}

	@Test(expected = Exception.class)
	public void testAdaugaCarte_TC5_EC() throws Exception {
		String titlu = "Carte Titlu";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 1899;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}

	@Test(expected = Exception.class)
	public void testAdaugaCarte_TC6_EC() throws Exception {
		String titlu = "Carte Titlu";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 2010;

		String editura = "";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}

	@Test(expected = Exception.class)
	public void testAdaugaCarte_TC7_EC() throws Exception {
		String titlu = "Carte Titlu";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 2010;

		String editura = "Editura";

		List<String> cuvinteCheie = null;

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}

	/* --------------- TC BV ------------------- */
	
	@Test
	public void testAdaugaCarte_TC3_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 2010;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}

	@Test
	public void testAdaugaCarte_TC4_BV() throws Exception {
		String titlu = "M";
		while (titlu.length() < 255)
			titlu += "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 2010;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}

	@Test
	public void testAdaugaCarte_TC5_BV() throws Exception {
		String titlu = "M";
		while (titlu.length() < 254)
			titlu += "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 2010;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}

	@Test
	public void testAdaugaCarte_TC9_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		// autori.add("Autor doi");

		int anAparitie = 2010;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}

	@Test
	public void testAdaugaCarte_TC10_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		while (autori.size() < 255)
			autori.add("Autor x");

		int anAparitie = 2010;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}
	
	@Test
	public void testAdaugaCarte_TC11_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		while (autori.size() < 254)
			autori.add("Autor x");

		int anAparitie = 2010;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}
	
	@Test
	public void testAdaugaCarte_TC12_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		while (autori.size() < 256)
			autori.add("Autor x");

		int anAparitie = 2010;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}
	
	@Test
	public void testAdaugaCarte_TC13_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 1900;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}
	
	@Test
	public void testAdaugaCarte_TC15_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 1901;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}
	
	@Test
	public void testAdaugaCarte_TC16_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = Year.now().getValue() - 1;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}
	
	@Test (expected = Exception.class)
	public void testAdaugaCarte_TC17_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = Year.now().getValue() + 1;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}
	
	@Test
	public void testAdaugaCarte_TC18_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = Year.now().getValue();

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}
	
	@Test
	public void testAdaugaCarte_TC21_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 2010;

		String editura = "M";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}
	
	@Test
	public void testAdaugaCarte_TC22_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 2010;

		String editura = "M";
		while (editura.length() < 255)
			editura += "M";
		
		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}
	
	@Test
	public void testAdaugaCarte_TC23_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 2010;

		String editura = "M";
		while (editura.length() < 254)
			editura += "M";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}
	
	@Test
	public void testAdaugaCarte_TC27_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 2010;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
//		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}
	
	@Test
	public void testAdaugaCarte_TC28_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 2010;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		while (cuvinteCheie.size() < 255)
			cuvinteCheie.add("c");
//		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}
	
	@Test
	public void testAdaugaCarte_TC29_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 2010;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		while (cuvinteCheie.size() < 254)
			cuvinteCheie.add("c");
//		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}
	
	@Test
	public void testAdaugaCarte_TC30_BV() throws Exception {
		String titlu = "M";

		List<String> autori = new ArrayList<String>();
		autori.add("Autor unu");
		autori.add("Autor doi");

		int anAparitie = 2010;

		String editura = "Editura";

		List<String> cuvinteCheie = new ArrayList<String>();
		cuvinteCheie.add("cuvant");
		while (cuvinteCheie.size() < 256)
			cuvinteCheie.add("c");
//		cuvinteCheie.add("cheie");

		ctrl.adaugaCarte(titlu, autori, anAparitie, editura, cuvinteCheie);

	}



}
