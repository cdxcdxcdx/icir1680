package biblioteca.control;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;

public class CartiRepoTest {

	CartiRepoMock repo;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@Before
	public void setUp() throws Exception {
		repo = new CartiRepoMock();
	}

	@Test
	public void testCautaCarte_Valid() {
		List<Carte> carti = repo.cautaCarteDupaAutor("Mihai");
		
		assert carti.size() > 0;
	}
	
	@Test(expected = Exception.class)
	public void testCautaCarte_Invalid() {
		repo.cautaCarteDupaAutor(null);
		assert false;
	}

}
