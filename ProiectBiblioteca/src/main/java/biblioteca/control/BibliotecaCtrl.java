package biblioteca.control;


import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.util.CarteValidator;

import java.util.List;

public class BibliotecaCtrl {

	private CartiRepoInterface cr;
	
	public BibliotecaCtrl(CartiRepoInterface cr){
		this.cr = cr;
	}
	
	public void adaugaCarte(String titlu, List<String> autori, int anAparitie, String editura, List<String> cuvinteCheie) throws Exception {
		Carte carte = new Carte();
		carte.setTitlu(titlu);
		carte.setAutori(autori);
		carte.setAnAparitie(anAparitie);
		carte.setEditura(editura);
		carte.setCuvinteCheie(cuvinteCheie);
		adaugaCarte(carte);
	}
	
	public void adaugaCarte(Carte c) throws Exception{
		CarteValidator.validateCarte(c);
		cr.adaugaCarte(c);
	}
	
	public void modificaCarte(Carte nou, Carte vechi) throws Exception{
		CarteValidator.validateCarte(nou);
		cr.modificaCarte(nou, vechi);
	}
	
	public void stergeCarte(Carte c) throws Exception{
		cr.stergeCarte(c);
	}

	public List<Carte> cautaCarte(String autor) throws Exception{
		CarteValidator.isStringCuvinte(autor);
		return cr.cautaCarteDupaAutor(autor);
	}
	
	public List<Carte> getCarti() throws Exception{
		return cr.getCarti();
	}
	
	public List<Carte> getCartiOrdonateDinAnul(int an) throws Exception{
		if(!CarteValidator.isAnValid(an))
			throw new Exception("Nu e numar!");
		return cr.getCartiOrdonateDinAnul(an);
	}
	
	
}
